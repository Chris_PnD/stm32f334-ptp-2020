/* Private includes ----------------------------------------------------------*/

#include "main.h"
#include "cmsis_os.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include <math.h>

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "bme280-interface.h"
#include "command_parser.h"
#include "uart_interface.h"

// Calibration values stored in ROM
/* ADC internal channels related definitions */
/* Internal voltage reference VrefInt */
#define VREFINT_CAL		(*((uint16_t*) ((uint32_t)0x1FFFF7BAU)))	/* Internal voltage reference, address of parameter VREFINT_CAL: VrefInt ADC raw data acquired at temperature 30 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV). */
#define VREFINT_CAL_VREF  ((uint32_t) 3300U)                    	/* Analog voltage reference (Vref+) value with which temperature sensor has been calibrated in production (tolerance: +-10 mV) (unit: mV). */
/* Temperature sensor */
#define TS_CAL1			(*((uint16_t*) ((uint32_t)0x1FFFF7B8U))) 	/* Internal temperature sensor, address of parameter TS_CAL1: On STM32F37x, temperature sensor ADC raw data acquired at temperature  25 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV). */
#define TS_CAL2 	   	(*((uint16_t*) ((uint32_t)0x1FFFF7C2U)))  	/* Internal temperature sensor, address of parameter TS_CAL2: On STM32F37x, temperature sensor ADC raw data acquired at temperature 110 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV). */
#define T_CAL1            (( int32_t)   30)                     	/* Internal temperature sensor, temperature at which temperature sensor has been calibrated in production for data into TEMPSENSOR_CAL1_ADDR (tolerance: +-5 DegC) (unit: DegC). */
#define T_CAL2            (( int32_t)  110)                     	/* Internal temperature sensor, temperature at which temperature sensor has been calibrated in production for data into TEMPSENSOR_CAL2_ADDR (tolerance: +-5 DegC) (unit: DegC). */
#define TEMPSENSOR_CAL_VREFANALOG          ((uint32_t) 3300U)       /* Analog voltage reference (Vref+) voltage with which temperature sensor has been calibrated in production (+-10 mV) (unit: mV). */

#define BUF_IN_SIZE 		30										/* Size of buffer for rx usart*/
#define BUF_OUT_SIZE 		30										/* Size of buffer for tx usart*/

/* Private typedef -----------------------------------------------------------*/


/* Static function prototypes --------------------------------------------------*/
static void init_irq_req(int cnt, int freq);
static void init_irq_adc_req(int cnt, int freq);
static void init_dma_req(int cnt, int freq);
static void init_dac_req(int cnt, int freq);
static void init_temp_req(int cnt, int freq);
static void init_mem_req(int cnt, int freq);
static void adc_cpl_dma_req(void);
static void adc_cpl_irq_req(void);
static void adc_cpl_temp_req(void);
static void timer_cb_req(TIM_HandleTypeDef *htim);
static void timer_bme_cb_req(TIM_HandleTypeDef *htim);
/* Constant variable -----------------------------------------------------------*/

const req_table_t req_table[6] ={
{		 IRQ_REQUEST, "irq", init_irq_adc_req,  adc_cpl_irq_req, NULL,				timer_cb_req,},
{		 DMA_REQUEST, "dma", init_dma_req,  	adc_cpl_dma_req, NULL,				NULL,},
{		 DAC_REQUEST, "dac", init_dac_req,  	NULL,			 NULL,				NULL,},
{TEMPERATURE_REQUEST, "temp",init_temp_req, 	NULL,			 adc_cpl_temp_req, 	timer_cb_req,},
{		 MEM_REQUEST, "mem", init_mem_req,  	NULL,			 NULL,				NULL,},
{		 BME_REQUEST, "bme", init_irq_req, 		NULL,			 NULL,				timer_bme_cb_req,}};
/* Private variable -----------------------------------------------------------*/

uint16_t* adc_buf_pt = NULL;
uint16_t* dac_buf_pt = NULL;

uint16_t dma_size = 0;

volatile uint16_t tim_cnt;

volatile req_type_t current_request_type;
char buf_out[BUF_OUT_SIZE];

/* External variable -----------------------------------------------------------*/

extern ADC_HandleTypeDef hadc1;
extern DAC_HandleTypeDef hdac1;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern osThreadId_t defaultTaskHandle;
extern DMA_HandleTypeDef hdma_adc1;

///////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// global functions ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

/*
 * user_parser function used for parse data received for USART
 */
void command_parser(uint8_t* buf_in)
{
	// table of command can be processed

	int cnt = 0,freq = 0;
	req_type_t i;

	// look for usart received command (buf_in) in cmd table
	for (i = 0; i < sizeof(req_table)/sizeof(req_table[0]); i++)
	{
		// compare received buf_in with command table
		if (strncasecmp((char*)buf_in, (char*)req_table[i].cmd_st, strlen((char*)req_table[i].cmd_st)) == 0)
		{
			// if found one try to obtain command numeric data parameters
			sscanf((char*)(buf_in + strlen((char*)req_table[i].cmd_st)),"%d,%d", &cnt, &freq);
			// transmit data to usart - echo for confirming command parsing
			sprintf((char*)buf_out, "%s:%d,%d\n", (char*)req_table[i].cmd_st, cnt, freq);
			uart_iterface_transmit(buf_out);
			// apply the appropriate function to the command
			if (req_table[i].init_fn != NULL)
			{
				current_request_type = i;
				req_table[i].init_fn(cnt, freq);
			}
		}
	}
}

const req_table_t* get_req_table_pt(void)
{
	return req_table;
}
////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// static function ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

static void init_irq_req(int cnt, int freq)
{
	// Prepare IRQ counter (command parameter cnt)
	tim_cnt = cnt - 1;

	// set timer 1 overflow period (command parameter freq)
	if (freq < 2)
		htim1.Init.Period = 1;
	else
		htim1.Init.Period = freq - 1;
	// Init hardware timer1 to trigger ADC
	HAL_TIM_Base_Init(&htim1);
	// start timer 1 with IT callback executed on timer/counter overflow
	HAL_TIM_Base_Start_IT(&htim1);
}

static void init_irq_adc_req(int cnt, int freq)
{
	// Prepare IRQ counter (command parameter cnt)
	tim_cnt = cnt - 1;

	// set timer 1 overflow period (command parameter freq)
	if (freq < 2)
		htim1.Init.Period = 1;
	else
		htim1.Init.Period = freq - 1;
	// Init hardware timer1 to trigger ADC
	HAL_TIM_Base_Init(&htim1);
	// start timer 1 with IT callback executed on timer/counter overflow
	HAL_TIM_Base_Start_IT(&htim1);
	// start adc with IT callback executed on ADC conversion complete
	HAL_ADC_Start_IT(&hadc1);
}

static void init_dma_req(int cnt, int freq)
{
	// we use DMA method to collect measured ADC data

	// set timer 1 overflow period (command parameter freq) - reset period value
	if (freq < 2)
		htim1.Init.Period = 1;
	else
		htim1.Init.Period = freq - 1;

	// stop adc IT mode
	HAL_ADC_Stop_IT(&hadc1);
	// Init hardware timer1 to trigger ADC on overflow event
	HAL_TIM_Base_Init(&htim1);
	// start hardware timer 1
	HAL_TIM_Base_Start(&htim1);


	// if allocated adc buffer freeing
	if (adc_buf_pt != NULL)
	{
		vPortFree(adc_buf_pt);
		adc_buf_pt = NULL;
	}
	if (adc_buf_pt == NULL)
	{
		// allocate memory space for collecting data (command parameter cnt)
		adc_buf_pt = pvPortMalloc(cnt * sizeof(uint16_t));
		// if adc_buf_pt == NULL than allocation failed (out of memory)
		if (adc_buf_pt != NULL)
		{
			dma_size = cnt;
			HAL_ADC_Start_DMA(&hadc1, (void*)adc_buf_pt, cnt);
		}
		else
		{
			// TODO: out of memory USART echo
		}
	}
}

static void init_dac_req(int cnt, int freq)
{
	// stop timer 2 - to reset period value
	HAL_TIM_Base_Stop(&htim2);

	// set timer 2 overflow period (command parameter freq)
	if (freq < 2)
		htim2.Init.Period = 1;
	else
		htim2.Init.Period = freq - 1;

	// Init hardware timer2 to trigger DAC on overflow event
	HAL_TIM_Base_Init(&htim2);
	// start hardware timer 2
	HAL_TIM_Base_Start(&htim2);

	// stop dma on dac
	HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_1);

	// if allocated dac buffer
	if (dac_buf_pt != NULL)
	{
		// free allocated memory
		vPortFree(dac_buf_pt);
		// set dac buffer to NULL
		dac_buf_pt = NULL;
	}
	// if dac buffer NOT allocated
	if (dac_buf_pt == NULL)
	{
		// allocate memory for DAC output buffer (command parameter cnt)
		dac_buf_pt = pvPortMalloc( cnt * sizeof(uint16_t));
		if (dac_buf_pt != NULL)
		{
			// Prepare output buffer filled by data of sine values of 1 period
			for (uint16_t i = 0; i < cnt; i++)
			{
				dac_buf_pt[i] = 0x7FF * ((double)1 + sin((double)i / cnt * 2 * 3.1415));
			}
			// Start DMA DAC Output generating
			HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (void *)dac_buf_pt, cnt, DAC_ALIGN_12B_R);
		}
	}
}

static void init_temp_req(int cnt, int freq)
{
	// we use IT method to obtain temperatur data
	// commented due to CUBEMX channel initialization

	//adc set temperature channel
	//The recommended sampling time is at least:
	//  - For devices STM32F37x: 17.1us for temperature sensor
	//	- For the other STM32F3 devices: 2.2us for each of channels Vbat/VrefInt/TempSensor.
	//64MHz x 17.1us = 1094,4
	//64MHz x 2,2us = 140,8
	//HAL_ADC_DeInit(&hadc1);
	//MX_ADC1_Init();
	//set_adc_channel(ADC_CHANNEL_VREFINT, ADC_SAMPLETIME_601CYCLES_5);
	//set_adc_channel(ADC_CHANNEL_TEMPSENSOR, ADC_SAMPLETIME_601CYCLES_5);

	// Prepare IRQ counter (command parameter cnt)
	tim_cnt = cnt - 1;

	// set timer 1 overflow period (command parameter freq)
	if (freq < 2)
		htim1.Init.Period = 1;
	else
		htim1.Init.Period = freq - 1;

	// Init hardware timer1 to trigger ADC on overflow event
	HAL_TIM_Base_Init(&htim1);
	// start timer 1 with IT callback executed on timer/counter overflow
	HAL_TIM_Base_Start_IT(&htim1);

	// OPTYMALIZATION hint
	// Injected mode init - where the ADC conversion can be "injected" during the conversion of regular channels due to some trigger
	// we do NOT need reconfigure adc setting if want only temperature data
	HAL_ADCEx_InjectedStart_IT(&hadc1);
}

static void init_mem_req(int cnt, int freq)
{
	// one shot free memory data request - debug purpose
	// read free heap space, minimum free heap size, highest level of memory
	// usage of default task
	sprintf((char*)buf_out, "Heap:%d,min:%d,task:%ld\n", xPortGetFreeHeapSize(),
			xPortGetMinimumEverFreeHeapSize(),
			uxTaskGetStackHighWaterMark( defaultTaskHandle ));
	// transmit command respond data by USART to user
	uart_iterface_transmit(buf_out);
}

static void adc_cpl_dma_req(void)
{
	// stop timer
	HAL_TIM_Base_Stop(&htim1);
	// callback is calling by DMA transfer complete
	// prepare collected data buffer in memory to send to user
	for (uint16_t i = 0; i < dma_size; i++)
	{
		// conversion from binary to float voltage value
		float volt = 3.3F * adc_buf_pt[i] / 0xFFF;

		// prepare send UART buffer of 1 data
		sprintf((char*)buf_out, "DMA%02d:%.3f[V]\n", i, volt);
		uart_iterface_transmit(buf_out);
	}
}

static void adc_cpl_irq_req(void)
{
	// callback is calling by ADC conversion complete
	// we collect data point after point
	uint16_t bin_val = HAL_ADC_GetValue(&hadc1);
	// conversion from binary to float voltage value
	float volt = 3.3 * bin_val / 0xFFF;
	// prepare send UART buffer of 1 data
	sprintf((char*)buf_out, "ADC%02d float:%0.3f[V]\n", (tim_cnt--), volt);
	uart_iterface_transmit(buf_out);
}

static void adc_cpl_temp_req(void)
{
	// configure temperature ADC channel in injection mode
	uint16_t bin_val_1 = HAL_ADCEx_InjectedGetValue(&hadc1, ADC_INJECTED_RANK_1);
	uint16_t bin_val_2 = HAL_ADCEx_InjectedGetValue(&hadc1, ADC_INJECTED_RANK_2);

	// converting temperature use factory coefficients pre calibration (see reference note)
	float a = (float)(T_CAL2 - T_CAL1) / (TS_CAL2 - TS_CAL1);
	// converting digital temperature data to float temperature in degrees Celsius
	float b = (float)T_CAL2 - a * TS_CAL2;
	// compensation Vref
	float temperature = a * (bin_val_1 * ((float)VREFINT_CAL / bin_val_2)) + b;

	// prepare send UART buffer of 1 data
	sprintf((char*)buf_out, "ADC%02d Temp:%0.2f[C]\n", (tim_cnt--), temperature);
	uart_iterface_transmit(buf_out);
}

static void timer_cb_req(TIM_HandleTypeDef *htim)
{
	if (tim_cnt)
	{
		// due to the hardware configuration of TRGO event,
		// (see: sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE)
		// the function HAL_ADC_Start_IT(&hadc1) does not have to be called manually
	}
	else
	{
		// if timer counter == 0 stop hardware timer 1
		HAL_TIM_Base_Stop_IT(&htim1);
	}
}

static void timer_bme_cb_req(TIM_HandleTypeDef *htim)
{
	if (tim_cnt == 0)
	{
		HAL_TIM_Base_Stop_IT(&htim1);
	}

	struct bme280_data bme280_data;
	if (bme280_measure(&bme280_data))
	{
		bme280_send_data(&bme280_data, tim_cnt--);
	}
}
