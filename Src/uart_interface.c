#include "main.h"
#include "cmsis_os.h"
#include "string.h"

#define IS_IRQ_MASKED()           (__get_PRIMASK() != 0U)
#define IS_IRQ_MODE()             (__get_IPSR() != 0U)

#define IS_IRQ()                  (IS_IRQ_MODE() || IS_IRQ_MASKED())


extern UART_HandleTypeDef huart2;
extern osSemaphoreId_t uart_tx_SemHandle;


void uart_iterface_transmit(char* buff)
{
	HAL_UART_Transmit_DMA( &huart2, (uint8_t *)buff, strlen(buff));
	// due to use not blocking function
	// we use semaphore to block send next USART buffer
	if (IS_IRQ())
	{
		while (osSemaphoreAcquire(uart_tx_SemHandle, 0) != osOK);
	}
	else
	{
		while (osSemaphoreAcquire(uart_tx_SemHandle, 100) != osOK);
	}
}
