
#define MAX_SIZE_OF_COMMAND_STR 5

/* Private typedef -----------------------------------------------------------*/

typedef enum req_type_ {
	IRQ_REQUEST,
	DMA_REQUEST,
	DAC_REQUEST,
	TEMPERATURE_REQUEST,
	MEM_REQUEST,
	BME_REQUEST,
}req_type_t;

typedef void(*req_init_func_t)(int cnt, int freq);
typedef void(*req_end_func_t)(void);
typedef void(*req_timer_func_t)(TIM_HandleTypeDef *htim);
typedef const char cmd_t[MAX_SIZE_OF_COMMAND_STR];

typedef struct req_table {
	req_type_t 			req_type;
	cmd_t				cmd_st;
	req_init_func_t 	init_fn;
	req_end_func_t 		adc_cpl_cb;
	req_end_func_t		adc_inj_cpl_cb;
	req_timer_func_t	timer_cpl_cb;
} req_table_t;

/* Public function  -----------------------------------------------------------*/

void command_parser(uint8_t* buf_in);
const req_table_t* get_req_table_pt(void);
