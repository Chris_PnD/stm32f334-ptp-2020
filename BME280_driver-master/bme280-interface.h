#include "bme280.h"
#include "stdbool.h"

uint8_t bme280_setup(void);
bool bme280_measure(struct bme280_data* bme280_data_pt);
void bme280_send_data(struct bme280_data* bme280_data_pt, uint16_t cnt);
