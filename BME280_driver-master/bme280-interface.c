
#include "cmsis_os.h"
#include <bme280.h>
#include <bme280_defs.h>
#include "uart_interface.h"
#include "main.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>


extern I2C_HandleTypeDef hi2c1;
extern UART_HandleTypeDef huart2;

uint8_t i2c_address = BME280_I2C_ADDR_PRIM;
struct bme280_dev bme_h;

//device bme280 I2C address
#define BME280_I2C_ADDRESS1                  (0x76 << 1)
#define BME280_I2C_ADDRESS2                  (0x77 << 1)


static void delay_us(uint32_t period, void *intf_ptr);
static int8_t bme_i2c_reg_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr);
static int8_t bme_i2c_reg_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr);
static uint8_t bmp280_read_byte(uint8_t addr);
static void bme280_interface_init(struct bme280_dev* bme);

uint8_t bme280_setup(void)
{
	//check I2C bus
	if (HAL_I2C_IsDeviceReady(&hi2c1, (uint8_t)(BME280_I2C_ADDRESS1), 3, 100) == HAL_OK)
	{
		uint8_t settings_sel;

		// read ID register to identify - yourself implementation
		uint8_t id = bmp280_read_byte(0xD0);

		// use driver provided by vendor
		bme280_interface_init(&bme_h);
		bme280_init(&bme_h);
		/* Recommended mode of operation: Indoor navigation */
		bme_h.settings.osr_h = BME280_OVERSAMPLING_1X;
		bme_h.settings.osr_p = BME280_OVERSAMPLING_16X;
		bme_h.settings.osr_t = BME280_OVERSAMPLING_2X;
		bme_h.settings.filter = BME280_FILTER_COEFF_16;

		settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;
		bme280_set_sensor_settings(settings_sel, &bme_h);
		return id;
	}
	return 0;
}

bool bme280_measure(struct bme280_data* bme280_data_pt)
{
	//check I2C bus
	if (HAL_I2C_IsDeviceReady(&hi2c1, (uint8_t)(BME280_I2C_ADDRESS1), 3, 100) == HAL_OK)
	{
		bme280_set_sensor_mode(BME280_FORCED_MODE, &bme_h);
		/* Wait for the measurement to complete and print data @25Hz */
		HAL_Delay(40);
		bme280_get_sensor_data(BME280_ALL, bme280_data_pt, &bme_h);
		return true;
	}
	return false;
}

void bme280_send_data(struct bme280_data* bme280_data_pt, uint16_t cnt)
{
	char buff[50];
	uint8_t t = bme280_data_pt->temperature;
	uint8_t tf = 100 * (bme280_data_pt->temperature - t);
	uint16_t p = 0.01 * bme280_data_pt->pressure;
	uint8_t pf = 100 * (0.01 * bme280_data_pt->pressure - p);
	uint8_t h = bme280_data_pt->humidity;
	uint8_t hf = 10 * (bme280_data_pt->humidity - h);

	// WARN: using float %f generating hard fault
	sprintf(buff, "BME%02d Temp:%d.%02d,Pres:%d.%02d,Humi:%d.%01d\n", cnt,t,tf,p,pf,h,hf);
	uart_iterface_transmit(buff);
}

static void bme280_interface_init(struct bme280_dev* bme)
{

	/* Map the delay function pointer with the function responsible for implementing the delay */
	bme->delay_us = delay_us;

	/* Assign device I2C address based on the status of SDO pin (GND for PRIMARY(0x76) & VDD for SECONDARY(0x77)) */
	bme->intf_ptr = (void *)&i2c_address;

	/* Select the interface mode as I2C */
	bme->intf = BME280_I2C_INTF;

	/* Map the I2C read & write function pointer with the functions responsible for I2C bus transfer */
	bme->read = bme_i2c_reg_read;
	bme->write = bme_i2c_reg_write;
}

/*!
 *  @brief Function that creates a mandatory delay required in some of the APIs such as "bmg250_soft_reset",
 *      "bmg250_set_foc", "bmg250_perform_self_test"  and so on.
 *
 *  @param[in] period_ms  : the required wait time in milliseconds.
 *  @return void.
 *
 */
static void delay_us(uint32_t period, void *intf_ptr)
{
    /* Implement the delay routine according to the target machine */
	// for non rtos implementation
	//HAL_Delay(period / 1000);
	// fro rtos implementation
	osDelay(period / 1000);
}

/*!
 *  @brief Function for writing the sensor's registers through I2C bus.
 *
 *  @param[in] i2c_addr : sensor I2C address.
 *  @param[in] reg_addr : Register address.
 *  @param[in] reg_data : Pointer to the data buffer whose value is to be written.
 *  @param[in] length   : No of bytes to write.
 *
 *  @return Status of execution
 *  @retval 0 -> Success
 *  @retval >0 -> Failure Info
 *
 */
static int8_t bme_i2c_reg_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr)
{
    /* Implement the I2C write routine according to the target machine. */
	return HAL_I2C_Mem_Write(&hi2c1, (*(uint8_t*)intf_ptr << 1), reg_addr, 1, (uint8_t*)data, len, 10);
}

/*!
 *  @brief Function for reading the sensor's registers through I2C bus.
 *
 *  @param[in] i2c_addr : Sensor I2C address.
 *  @param[in] reg_addr : Register address.
 *  @param[out] reg_data    : Pointer to the data buffer to store the read data.
 *  @param[in] length   : No of bytes to read.
 *
 *  @return Status of execution
 *  @retval 0 -> Success
 *  @retval >0 -> Failure Info
 *
 */
static int8_t bme_i2c_reg_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr)
{
    /* Implement the I2C read routine according to the target machine. */
	return HAL_I2C_Mem_Read(&hi2c1, (*(uint8_t*)intf_ptr << 1), reg_addr, 1, data, len, 10);
}

static uint8_t bmp280_read_byte(uint8_t addr)
{
	uint8_t byte = 0;

	// read one byte from address in polling procedure
	HAL_I2C_Mem_Read(&hi2c1, BME280_I2C_ADDRESS1, addr, 1, &byte, 1, 10);

return byte;
}

