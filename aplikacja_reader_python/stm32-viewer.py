import tkinter as tk
from typing import List, Any

import serial
import serial.tools.list_ports

from tkinter import ttk, Tk
from matplotlib.backends.backend_tkagg import (FigureCanvas)
from matplotlib.figure import Figure

def _quit():
    root.quit()  # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent


def _write_dac_data():
    s = serial.Serial(port=combobox1.get(), baudrate=115200)
    print("Write serial port")
    s.write(b'dac 200,10\n')
    print(s.readline())
    s.close()


def _read_data():
    s = serial.Serial(port=combobox1.get(), baudrate=115200)
    print("Read serial port")
    s.write(b'irq 200,10\n')
    print(s.readline())
    y = []

    for i in range(200):
        ser_string = s.readline().decode('utf-8')
        float_pos = ser_string.find('float') + 6
        float_str = ser_string[float_pos:float_pos + 5]
        # print(float(float_str))
        data_y = float(float_str)
        y.append(data_y)
    x = range(len(y))

    s.close()
    data.clear()
    data.plot(x, y)
    canvas.draw()
    data.set_title('ADC chart[V]')


root: Tk = tk.Tk()
root.wm_title("STM32 data viewer")

fig = Figure(figsize=(5, 2), dpi=80)
data = fig.add_subplot(111)

canvas = FigureCanvas(fig, master=root)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)


button1 = tk.Button(master=root, text="STM32 Read Data", command=_read_data)
button2 = tk.Button(master=root, text="STM32 DAC Init", command=_write_dac_data)
button3 = tk.Button(master=root, text="QUIT", command=_quit)
button3.pack(side=tk.BOTTOM)
button2.pack(side=tk.BOTTOM)
button1.pack(side=tk.BOTTOM)
combobox1 = ttk.Combobox(root)
combobox1.pack(side=tk.BOTTOM)

ports = serial.tools.list_ports.comports()
list_cb: List[Any] = []
for port, desc, hwid in sorted(ports):
    list_cb.append(port)
    print("{}: {} [{}]".format(port, desc, hwid))

combobox1['values'] = list_cb
combobox1.set(port)

tk.mainloop()
